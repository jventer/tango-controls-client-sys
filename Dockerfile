FROM artefact.skao.int/ska-tango-images-tango-cpp:9.3.11

USER root

RUN apt-get update && apt-get install -y build-essential curl clang git pkg-config python3
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
RUN /root/.cargo/bin/cargo install bindgen-cli
RUN git clone https://gitlab.com/tango-controls/cppTango.git /cppTango

