use crate::bridge::database_ffi;
use anyhow::Result;
use cxx::UniquePtr;

pub struct DatabaseProxy {
    database: UniquePtr<database_ffi::BridgedDatabase>,
}

impl DatabaseProxy {
    pub fn new() -> Result<Self> {
        let db = database_ffi::new_database()?;
        Ok(DatabaseProxy { database: db })
    }

    pub fn get_device_exported(&self, filter: &str) -> Result<Vec<String>> {
        let devices = self.database.get_device_exported(filter)?;
        Ok(devices)
    }
}
