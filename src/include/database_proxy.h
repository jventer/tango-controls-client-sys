#ifndef SRC_INCLUDE_database_H_
#define SRC_INCLUDE_database_H_

// #pragma once
#include <tango.h>
#include <rust/cxx.h>
#include <memory>


class BridgedDatabase {

public:
  BridgedDatabase();
  ~BridgedDatabase();
  rust::vec<rust::string> get_device_exported(rust::Str) const;

private:
  class impl;
  std::shared_ptr<impl> impl;
  Tango::Database* database;
};

std::unique_ptr<BridgedDatabase> new_database();

#endif  // SRC_INCLUDE_database_H_
