#pragma once
#include <string>
#include <tango.h>

namespace rust {
    namespace behavior {
        template <typename Try, typename Fail>
        void trycatch(Try&& func, Fail&& fail) noexcept try {
            func();
        }
        catch (Tango::ConnectionFailed& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::CommunicationFailed& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::WrongNameSyntax& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::NonDbDevice& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::WrongData& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::NonSupportedFeature& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::AsynCall& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::AsynReplyNotArrived& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::EventSystemFailed& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::DeviceUnlocked& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (Tango::NotAllowed& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        // catch (Tango::ProgrammingError  & ex) {
        //     // Tango::Except::print_exception(ex);
        //     std::string reason(ex.errors[0].reason.in());
        //     fail(reason.c_str());
        // }
        catch (Tango::DevFailed& ex) {
            // Tango::Except::print_exception(ex);
            std::string reason(ex.errors[0].reason.in());
            fail(reason.c_str());
        }
        catch (std::exception& ex) {
            fail(ex.what());
        }
        catch (int i) {
            auto msg = std::to_string(i);
            fail(msg.c_str());
        }
        catch (std::string& ex) {
            fail(ex.c_str());
        }
        catch (...) {
            std::exception_ptr p = std::current_exception();
            std::cerr << "Error ID" << (p ? p.__cxa_exception_type()->name() : "null") << std::endl;
            fail("Logged an unknown error");
        }
    }
}