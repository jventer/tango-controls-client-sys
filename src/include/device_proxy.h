#ifndef SRC_INCLUDE_device_proxy_H_
#define SRC_INCLUDE_device_proxy_H_

// #pragma once
#include <tango.h>
#include <rust/cxx.h>
#include <memory>


enum class BridgedBool : ::std::uint8_t;
enum class DispLevel : ::std::uint8_t;
enum class AttrWriteType : ::std::uint8_t;
enum class DevState : ::std::uint8_t;
enum class BridgedCmdArgType : ::std::uint8_t;
struct BridgedCommandInfo;
struct BridgedDeviceData;
struct BridgedCmdArgTypeData;
struct BridgedAttributeInfo;
struct BridgedAttributeData;


class BridgedDeviceProxy {

public:
  BridgedDeviceProxy(rust::str device_name);
  ~BridgedDeviceProxy();
  int32_t ping() const;
  DevState state() const;
  rust::string status() const;
  BridgedCommandInfo command_query(rust::str command_name) const;
  BridgedCmdArgTypeData command_inout(rust::str command_name, BridgedCmdArgTypeData bridged_command_data) const;
  rust::vec<rust::string> get_command_list() const;
  rust::vec<rust::string> get_attribute_list() const;
  BridgedAttributeData read_attribute(rust::str attribute_name) const;
  rust::vec<BridgedAttributeInfo> attribute_list_query() const;
  rust::vec<BridgedCommandInfo> command_list_query() const;

private:
  class impl;
  std::shared_ptr<impl> impl;
  Tango::DeviceProxy* device_proxy;
};

std::unique_ptr<BridgedDeviceProxy> new_device_proxy(rust::str);

#endif  // SRC_INCLUDE_device_proxy_H_
