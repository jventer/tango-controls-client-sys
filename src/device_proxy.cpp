#include <tango.h>
#include "rust/cxx.h"
#include <tango-controls-client-sys/src/bridge.rs.h>

class BridgedDeviceProxy::impl {
  friend BridgedDeviceProxy;
  Tango::DeviceProxy* device_proxy = 0;
};

BridgedDeviceProxy::BridgedDeviceProxy(rust::str device_name) : impl(new class BridgedDeviceProxy::impl) {
  device_proxy = new Tango::DeviceProxy(std::string(device_name));
}

BridgedDeviceProxy::~BridgedDeviceProxy() {
  if (device_proxy) {
    delete device_proxy;
  }
}

int32_t BridgedDeviceProxy::ping(void) const {
  int32_t res = device_proxy->ping();
  return res;
}

DevState BridgedDeviceProxy::state(void) const {
  Tango::DevState result = device_proxy->state();
  return (DevState)result;
}

rust::string BridgedDeviceProxy::status() const {
  std::string result = device_proxy->status();
  return (rust::string)result;
}

BridgedCommandInfo BridgedDeviceProxy::command_query(rust::Str command_name) const {
  std::string str_command_name;
  str_command_name = (std::string)command_name;
  Tango::CommandInfo result = device_proxy->command_query(str_command_name);
  BridgedCommandInfo tango_command_info{};
  tango_command_info.cmd_name = (rust::string)result.cmd_name;
  tango_command_info.in_type_desc = (rust::string)result.in_type_desc;
  tango_command_info.out_type_desc = (rust::string)result.out_type_desc;
  tango_command_info.cmd_tag = (int64_t)result.cmd_tag;
  tango_command_info.in_type = (CmdArgType)result.in_type;
  tango_command_info.out_type = (CmdArgType)result.out_type;
  tango_command_info.disp_level = (DispLevel)result.disp_level;
  return tango_command_info;
}

BridgedCmdArgTypeData BridgedDeviceProxy::command_inout(rust::Str command_name, BridgedCmdArgTypeData bridged_command_data) const {
  Tango::DeviceData din;
  Tango::DeviceData dout;
  BridgedCmdArgTypeData command_result;

  // Build up DeviceData input
  switch (bridged_command_data.populated_command_arg_type)
  {

    case  CmdArgType::DevVoid:
    {
      device_proxy->command_inout((std::string)command_name);
      command_result.populated_command_arg_type = CmdArgType::DevVoid;
      command_result.data_Void = true;
      return command_result;
    }

    case CmdArgType::DevBoolean:
    {
      switch (bridged_command_data.data_Boolean)
      {
      case BridgedBool::BridgedTrue:
        din << true;
        break;
      case BridgedBool::BridgedFalse:
        din << false;
        break;
      }
      break;
    }

    case CmdArgType::DevShort:
      din << bridged_command_data.data_Short;
      break;

    case CmdArgType::DevUShort:
      din << bridged_command_data.data_UShort;
      break;

    case CmdArgType::DevLong:
      din << bridged_command_data.data_Long;
      break;

    case CmdArgType::DevULong:
      din << bridged_command_data.data_ULong;
      break;

    case CmdArgType::DevLong64:
      din << bridged_command_data.data_Long64;
      break;

    case CmdArgType::DevULong64:
      din << bridged_command_data.data_ULong64;
      break;

    case CmdArgType::DevFloat:
      din << bridged_command_data.data_Float;
      break;

    case CmdArgType::DevDouble:
      din << bridged_command_data.data_Double;
      break;

    case CmdArgType::DevString:
      din << (std::string)bridged_command_data.data_String;
      break;

    case CmdArgType::DevUChar:
      din << bridged_command_data.data_UChar;
      break;

    case CmdArgType::DevVarCharArray:
    {
      std::size_t var_char_arr_size = bridged_command_data.data_CharArray.size();
      Tango::DevVarCharArray* var_char_arr = new Tango::DevVarCharArray();
      var_char_arr->length(var_char_arr_size);
      for (unsigned int i = 0; i < var_char_arr_size; i++) {
        (*var_char_arr)[i] = bridged_command_data.data_CharArray[i];
      }
      din << var_char_arr;
      break;
    }

    case CmdArgType::DevVarStringArray:
    {
      unsigned int var_str_arr_size = bridged_command_data.data_StringArray.size();
      std::vector<std::string> tmp_string_arr;
      for (unsigned int i = 0; i < var_str_arr_size; i++) {
        tmp_string_arr.push_back((std::string)bridged_command_data.data_StringArray[i]);
      }
      din << tmp_string_arr;
      break;
    }

    case CmdArgType::DevVarShortArray:
    {
      std::vector<short> short_array;
      for (int16_t i : bridged_command_data.data_ShortArray) {
        short_array.push_back((short)i);
      }
      din << short_array;
      break;
    }

    case CmdArgType::DevVarUShortArray:
    {
      std::vector<unsigned short> ushort_array;
      for (u_int16_t i : bridged_command_data.data_UShortArray) {
        ushort_array.push_back((unsigned short)i);
      }
      din << ushort_array;
      break;
    }

    case CmdArgType::DevVarLongArray:
    {
      unsigned int long_arr_size = bridged_command_data.data_LongArray.size();
      Tango::DevVarLongArray* var_long_arr = new Tango::DevVarLongArray();
      var_long_arr->length(long_arr_size);
      for (unsigned int i = 0; i < long_arr_size; i++) {
        (*var_long_arr)[i] = bridged_command_data.data_LongArray[i];
      }
      din << var_long_arr;
      break;
    }

    case CmdArgType::DevVarULongArray:
    {
      unsigned int ulong_arr_size = bridged_command_data.data_ULongArray.size();
      Tango::DevVarULongArray* var_ulong_arr = new Tango::DevVarULongArray();
      var_ulong_arr->length(ulong_arr_size);
      for (unsigned int i = 0; i < ulong_arr_size; i++) {
        (*var_ulong_arr)[i] = bridged_command_data.data_ULongArray[i];
      }
      din << var_ulong_arr;
      break;
    }

    case CmdArgType::DevVarLong64Array:
    {
      unsigned int long64_arr_size = bridged_command_data.data_Long64Array.size();
      Tango::DevVarLong64Array* var_long64_arr = new Tango::DevVarLong64Array();
      var_long64_arr->length(long64_arr_size);
      for (unsigned int i = 0; i < long64_arr_size; i++) {
        (*var_long64_arr)[i] = bridged_command_data.data_Long64Array[i];
      }
      din << var_long64_arr;
      break;
    }

    case CmdArgType::DevVarULong64Array:
    {
      unsigned int ulong64_arr_size = bridged_command_data.data_ULong64Array.size();
      Tango::DevVarULong64Array* var_ulong64_arr = new Tango::DevVarULong64Array();
      var_ulong64_arr->length(ulong64_arr_size);
      for (unsigned int i = 0; i < ulong64_arr_size; i++) {
        (*var_ulong64_arr)[i] = bridged_command_data.data_ULong64Array[i];
      }
      din << var_ulong64_arr;
      break;
    }

    case CmdArgType::DevVarFloatArray:
    {
      unsigned int float_arr_size = bridged_command_data.data_FloatArray.size();
      Tango::DevVarFloatArray* var_float_arr = new Tango::DevVarFloatArray();
      var_float_arr->length(float_arr_size);
      for (unsigned int i = 0; i < float_arr_size; i++) {
        (*var_float_arr)[i] = bridged_command_data.data_FloatArray[i];
      }
      din << var_float_arr;
      break;
    }

    case CmdArgType::DevVarDoubleArray:
    {
      unsigned int double_arr_size = bridged_command_data.data_DoubleArray.size();
      Tango::DevVarDoubleArray* var_double_arr = new Tango::DevVarDoubleArray();
      var_double_arr->length(double_arr_size);
      for (unsigned int i = 0; i < double_arr_size; i++) {
        (*var_double_arr)[i] = bridged_command_data.data_DoubleArray[i];
      }
      din << var_double_arr;
      break;
    }

    case CmdArgType::DevVarLongStringArray:
    {

      Tango::DevVarLongStringArray var_long_string_arr;
      // long array
      unsigned int long_arr_size = bridged_command_data.data_LongArray.size();
      var_long_string_arr.lvalue.length(long_arr_size);
      for (unsigned int i = 0; i < long_arr_size; i++) {
        var_long_string_arr.lvalue[i] = bridged_command_data.data_LongArray[i];
      }

      // string array
      unsigned int var_str_arr_size = bridged_command_data.data_StringArray.size();
      var_long_string_arr.svalue.length(var_str_arr_size);
      for (unsigned int i = 0; i < var_str_arr_size; i++) {
        var_long_string_arr.svalue[i] = bridged_command_data.data_StringArray[i].c_str();
      }
      din << var_long_string_arr;
      break;
    }

    case CmdArgType::DevVarDoubleStringArray:
    {

      Tango::DevVarDoubleStringArray var_double_string_arr;
      // double array
      unsigned int double_arr_size = bridged_command_data.data_DoubleArray.size();
      var_double_string_arr.dvalue.length(double_arr_size);
      for (unsigned int i = 0; i < double_arr_size; i++) {
        var_double_string_arr.dvalue[i] = bridged_command_data.data_DoubleArray[i];
      }

      // string array
      unsigned int var_str_arr_size = bridged_command_data.data_StringArray.size();
      var_double_string_arr.svalue.length(var_str_arr_size);
      for (unsigned int i = 0; i < var_str_arr_size; i++) {
        var_double_string_arr.svalue[i] = bridged_command_data.data_StringArray[i].c_str();
      }
      din << var_double_string_arr;
      break;
    }

    default:
      throw (std::string)"No matches for populated_device_data";
    }

    dout = device_proxy->command_inout((std::string)command_name, din);
    int data_type = dout.get_type();

    // Build up response
    switch (data_type)
    {
      case Tango::DEV_BOOLEAN:
      {
        command_result.populated_command_arg_type = CmdArgType::DevBoolean;
        bool bool_result;
        dout >> bool_result;
        BridgedBool bridged_bool;
        switch (bool_result)
        {
        case false:
          bridged_bool = BridgedBool::BridgedFalse;
          break;

        case true:
          bridged_bool = BridgedBool::BridgedTrue;
          break;
        }
        command_result.data_Boolean = bridged_bool;
        return command_result;
      }

      case Tango::DEVVAR_BOOLEANARRAY: 
      {

        command_result.populated_command_arg_type = CmdArgType::DevVarBooleanArray;
        std::vector<bool> bool_arr_result;
        dout >> bool_arr_result;
        rust::Vec<BridgedBool> bool_arr;

        for (bool cc : bool_arr_result) {
          if (cc == true)
          {
            bool_arr.push_back(BridgedBool::BridgedTrue);
          }
          else
          {
            bool_arr.push_back(BridgedBool::BridgedFalse);
          }
        }
        command_result.data_BooleanArray = bool_arr;
        return command_result;
      }

      case Tango::DEV_SHORT:
      {
        command_result.populated_command_arg_type = CmdArgType::DevShort;
        int16_t short_result;
        dout >> short_result;
        command_result.data_Short = (int16_t)short_result;
        return command_result;
      }

      case Tango::DEV_USHORT:
      {
        command_result.populated_command_arg_type = CmdArgType::DevUShort;
        unsigned short ushort_result;
        dout >> ushort_result;
        command_result.data_UShort = (unsigned short)ushort_result;
        return command_result;
      }

      case Tango::DEV_LONG:
      {
        command_result.populated_command_arg_type = CmdArgType::DevLong;
        int32_t long_result;
        dout >> long_result;
        command_result.data_Long = (int32_t)long_result;
        return command_result;
      }

      case Tango::DEV_ULONG:
      {
        command_result.populated_command_arg_type = CmdArgType::DevULong;
        Tango::DevULong ulong_result;
        dout >> ulong_result;
        command_result.data_ULong = (uint32_t)ulong_result;
        return command_result;
      }

      case Tango::DEV_LONG64:
      {
        command_result.populated_command_arg_type = CmdArgType::DevLong64;
        Tango::DevLong64 long64_result;
        dout >> long64_result;
        command_result.data_Long64 = (int64_t)long64_result;
        return command_result;
      }

      case Tango::DEV_ULONG64:
      {
        command_result.populated_command_arg_type = CmdArgType::DevULong64;
        Tango::DevULong64 ulong64_result;
        dout >> ulong64_result;
        command_result.data_ULong64 = (uint64_t)ulong64_result;
        return command_result;
      }

      // case Tango::DevState:
      // {
      //   command_result.populated_device_data = CmdArgType::DevState;
      //   unsigned long long ulong_result;
      //   dout >> ulong_result;
      //   command_result.data_Long = (unsigned long long) ulong_result;
      //   return command_result;
      // }

      // case Tango::DevEncoded:
      //   command_result.populated_device_data = CmdArgType::DevDevEncoded;
      //   unsigned long long ulong_result;
      //   dout >> ulong_result;
      //   command_result.data_Long = (unsigned long long) ulong_result;
      //   return command_result;

      case Tango::DEV_FLOAT:
      {
        command_result.populated_command_arg_type = CmdArgType::DevFloat;
        float float_result;
        dout >> float_result;
        command_result.data_Float = (float)float_result;
        return command_result;
      }

      case Tango::DEV_DOUBLE:
      {
        command_result.populated_command_arg_type = CmdArgType::DevDouble;
        double double_result;
        dout >> double_result;
        command_result.data_Double = (double)double_result;
        return command_result;
      }

      case Tango::DEV_STRING:
      {
        command_result.populated_command_arg_type = CmdArgType::DevString;
        std::string str_result;
        dout >> str_result;
        command_result.data_String = (std::string)str_result;
        return command_result;
      }

      case Tango::DEV_UCHAR:
      {
        command_result.populated_command_arg_type = CmdArgType::DevUChar;
        std::string char_result;
        dout >> char_result;
        command_result.data_CharArray.push_back(char_result.c_str()[0]);
        return command_result;
      }

      case Tango::DEVVAR_CHARARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarCharArray;
        std::vector<u_int8_t> char_arr_result;
        dout >> char_arr_result;
        rust::Vec<rust::u8> u8_arr;

        for (rust::u8 cc : char_arr_result) {
          u8_arr.push_back(cc);
        }
        command_result.data_CharArray = u8_arr;
        return command_result;
      }

      case Tango::DEVVAR_STRINGARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarStringArray;
        std::vector<std::string> string_arr_result;
        dout >> string_arr_result;
        rust::Vec<rust::string> string_arr;

        for (rust::string cc : string_arr_result) {
          string_arr.push_back(cc);
        }
        command_result.data_StringArray = string_arr;
        return command_result;
      }

      case Tango::DEVVAR_SHORTARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarShortArray;
        std::vector<int16_t> short_arr_result;
        dout >> short_arr_result;
        rust::Vec<int16_t> short_arr;

        for (int16_t cc : short_arr_result) {
          short_arr.push_back(cc);
        }
        command_result.data_ShortArray = short_arr;
        return command_result;
      }

      case Tango::DEVVAR_USHORTARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarUShortArray;
        std::vector<u_int16_t> ushort_arr_result;
        dout >> ushort_arr_result;
        rust::Vec<u_int16_t> ushort_arr;

        for (u_int16_t cc : ushort_arr_result) {
          ushort_arr.push_back(cc);
        }
        command_result.data_UShortArray = ushort_arr;
        return command_result;
      }

      case Tango::DEVVAR_LONGARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarLongArray;
        std::vector<int32_t> long_arr_result;
        dout >> long_arr_result;
        rust::Vec<int32_t> long_arr;

        for (int32_t cc : long_arr_result) {
          long_arr.push_back(cc);
        }
        command_result.data_LongArray = long_arr;
        return command_result;
      }

      case Tango::DEVVAR_ULONGARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarULongArray;
        std::vector<u_int32_t> ulong_arr_result;
        dout >> ulong_arr_result;
        rust::Vec<u_int32_t> ulong_arr;

        for (u_int32_t cc : ulong_arr_result) {
          ulong_arr.push_back(cc);
        }
        command_result.data_ULongArray = ulong_arr;
        return command_result;
      }

      case Tango::DEVVAR_LONG64ARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarLong64Array;
        std::vector<int64_t> long64_arr_result;
        dout >> long64_arr_result;
        rust::Vec<int64_t> long64_arr;

        for (int64_t cc : long64_arr_result) {
          long64_arr.push_back(cc);
        }
        command_result.data_Long64Array = long64_arr;
        return command_result;
      }

      case Tango::DEVVAR_ULONG64ARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarULong64Array;
        std::vector<u_int64_t> ulong64_arr_result;
        dout >> ulong64_arr_result;
        rust::Vec<u_int64_t> ulong64_arr;

        for (u_int64_t cc : ulong64_arr_result) {
          ulong64_arr.push_back(cc);
        }
        command_result.data_ULong64Array = ulong64_arr;
        return command_result;
      }

      case Tango::DEVVAR_FLOATARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarFloatArray;
        std::vector<float> float_arr_result;
        dout >> float_arr_result;
        rust::Vec<float> float_arr;

        for (float cc : float_arr_result) {
          float_arr.push_back(cc);
        }
        command_result.data_FloatArray = float_arr;
        return command_result;
      }

      case Tango::DEVVAR_DOUBLEARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarDoubleArray;
        std::vector<double> double_arr_result;
        dout >> double_arr_result;
        rust::Vec<double> double_arr;

        for (double cc : double_arr_result) {
          double_arr.push_back(cc);
        }
        command_result.data_DoubleArray = double_arr;
        return command_result;
      }

      case Tango::DEVVAR_LONGSTRINGARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarLongStringArray;

        const Tango::DevVarLongStringArray* long_str_res;
        dout >> long_str_res;

        rust::Vec<int32_t> long_arr;
        rust::Vec<rust::string> string_arr;

        // long arr
        for (unsigned int i = 0; i < long_str_res->lvalue.length(); i++) {
          long_arr.push_back((int32_t)long_str_res->lvalue[i]);
        }

        // string arr
        for (unsigned int i = 0; i < long_str_res->svalue.length(); i++) {
          string_arr.push_back((rust::string)long_str_res->svalue[i]);
        }

        command_result.data_LongArray = long_arr;
        command_result.data_StringArray = string_arr;
        return command_result;
      }

      case Tango::DEVVAR_DOUBLESTRINGARRAY:
      {
        command_result.populated_command_arg_type = CmdArgType::DevVarDoubleStringArray;
        rust::Vec<double> double_arr;
        rust::Vec<rust::string> string_arr;

        const Tango::DevVarDoubleStringArray* double_str_res;
        dout >> double_str_res;

        // double arr
        for (unsigned int i = 0; i < double_str_res->dvalue.length(); i++) {
          double_arr.push_back(double_str_res->dvalue[i]);
        }

        // string arr
        for (unsigned int i = 0; i < double_str_res->svalue.length(); i++) {
          string_arr.push_back((rust::string)double_str_res->svalue[i]);
        }

        command_result.data_DoubleArray = double_arr;
        command_result.data_StringArray = string_arr;
        return command_result;
      }

      default:
        throw (std::string)"No type match";
  }
}


rust::vec<rust::string> BridgedDeviceProxy::get_command_list() const {
  std::vector<std::string> command_list;
  rust::vec<rust::string> command_names;
  command_list = *device_proxy->get_command_list();
  for (std::string co : command_list) {
    command_names.push_back((rust::string)co);
  }
  return command_names;
}

rust::vec<rust::string> BridgedDeviceProxy::get_attribute_list() const {
  std::vector<std::string> attribute_list;
  rust::vec<rust::string> attribute_names;
  attribute_list = *device_proxy->get_attribute_list();
  for (std::string att : attribute_list) {
    attribute_names.push_back((rust::string)att);
  }
  return attribute_names;
}

BridgedAttributeData BridgedDeviceProxy::read_attribute(rust::Str attribute_name) const {
  Tango::DeviceAttribute device_attribute;
  BridgedCmdArgTypeData bridged_attribute_value;
  BridgedAttributeData bridged_attribute_data;

  device_attribute = device_proxy->read_attribute((std::string)attribute_name);
  Tango::DevErrorList error_list = device_attribute.err_list.inout();

  if (error_list.length() > 0) {
    throw (std::string)error_list[0].desc;
  }

  int data_type = device_attribute.get_type();
  Tango::AttrDataFormat attr_data_format = device_attribute.get_data_format();

  if (attr_data_format == Tango::AttrDataFormat::IMAGE) {
    throw (std::string)"tango.AttrDataFormat.IMAGE not supported";
  }

  if (attr_data_format == Tango::AttrDataFormat::FMT_UNKNOWN) {
    throw (std::string)"tango.AttrDataFormat.FMT_UNKNOWN not supported";
  }

  bridged_attribute_data.format = (AttrDataFormat)attr_data_format;
  bridged_attribute_data.quality = (AttrQuality)device_attribute.quality;
  bridged_attribute_data.name = (rust::string)device_attribute.name;
  bridged_attribute_data.dim_x = device_attribute.dim_x;
  bridged_attribute_data.dim_y = device_attribute.dim_y;
  bridged_attribute_data.time_stamp_tv_sec = device_attribute.time.tv_sec;
  bridged_attribute_data.time_stamp_tv_usec = device_attribute.time.tv_usec;
  bridged_attribute_data.time_stamp_tv_nsec = device_attribute.time.tv_nsec;

  // Build up response
  // Swith tango_const.h CmdArgType
  switch (data_type)
  {


  case Tango::DEV_BOOLEAN:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevBoolean;
      bool bool_result;
      device_attribute >> bool_result;
      if (bool_result == true) {
        bridged_attribute_value.data_Boolean = BridgedBool::BridgedTrue;
      }
      else {
        bridged_attribute_value.data_Boolean = BridgedBool::BridgedFalse;
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarBooleanArray;
      std::vector<bool> bool_vec;
      device_attribute >> bool_vec;
      bool_vec.pop_back();
      for (bool b : bool_vec) {
        if (b) {
          bridged_attribute_value.data_BooleanArray.push_back(BridgedBool::BridgedTrue);
        }
        else {
          bridged_attribute_value.data_BooleanArray.push_back(BridgedBool::BridgedFalse);
        }
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_UCHAR:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevUChar;
      uint8_t uchar_result;
      device_attribute >> uchar_result;
      bridged_attribute_value.data_UChar = uchar_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarCharArray;

      std::vector<uint8_t> uchar_vec;
      device_attribute >> uchar_vec;
      uchar_vec.pop_back();

      for (uint8_t i : uchar_vec) {
        bridged_attribute_value.data_CharArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_SHORT:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevShort;
      int16_t short_result;
      device_attribute >> short_result;
      bridged_attribute_value.data_Short = short_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarShortArray;
      std::vector<int16_t> short_vec;
      device_attribute >> short_vec;
      short_vec.pop_back();
      for (int16_t i : short_vec) {
        bridged_attribute_value.data_ShortArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_USHORT:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevUShort;
      u_int16_t ushort_result;
      device_attribute >> ushort_result;
      bridged_attribute_value.data_UShort = ushort_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarUShortArray;
      std::vector<u_int16_t> ushort_vec;
      device_attribute >> ushort_vec;
      ushort_vec.pop_back();
      for (u_int16_t i : ushort_vec) {
        bridged_attribute_value.data_UShortArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_LONG:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevLong;
      int32_t long_result;
      device_attribute >> long_result;
      bridged_attribute_value.data_Long = long_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarLongArray;
      std::vector<int32_t> long_vec;
      device_attribute >> long_vec;
      long_vec.pop_back();
      for (int32_t i : long_vec) {
        bridged_attribute_value.data_LongArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_ULONG:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevULong;
      u_int32_t ulong_result;
      device_attribute >> ulong_result;
      bridged_attribute_value.data_ULong = ulong_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarULongArray;
      std::vector<u_int32_t> ulong_vec;
      device_attribute >> ulong_vec;
      for (u_int32_t i : ulong_vec) {
        bridged_attribute_value.data_ULongArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_LONG64:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevLong64;
      int64_t long64_result;
      device_attribute >> long64_result;
      bridged_attribute_value.data_Long64 = long64_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarLong64Array;
      std::vector<int64_t> long64_vec;
      device_attribute >> long64_vec;
      for (int64_t i : long64_vec) {
        bridged_attribute_value.data_Long64Array.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_ULONG64:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevULong64;
      u_int64_t ulong64_result;
      device_attribute >> ulong64_result;
      bridged_attribute_value.data_ULong64 = ulong64_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarULong64Array;
      std::vector<u_int64_t> ulong64_vec;
      device_attribute >> ulong64_vec;
      for (u_int64_t i : ulong64_vec) {
        bridged_attribute_value.data_ULong64Array.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_FLOAT:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevFloat;
      float float_result;
      device_attribute >> float_result;
      bridged_attribute_value.data_Float = float_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarFloatArray;
      std::vector<float> float_vec;
      device_attribute >> float_vec;
      float_vec.pop_back();
      for (float i : float_vec) {
        bridged_attribute_value.data_FloatArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_DOUBLE:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevDouble;
      double double_result;
      device_attribute >> double_result;
      bridged_attribute_value.data_Double = double_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarDoubleArray;
      std::vector<double> double_vec;
      device_attribute >> double_vec;
      double_vec.pop_back();
      for (double i : double_vec) {
        bridged_attribute_value.data_DoubleArray.push_back(i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_STRING:
  {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevString;
      std::string str_result;
      device_attribute >> str_result;
      bridged_attribute_value.data_String = (rust::string)str_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarStringArray;
      std::vector<std::string> string_vec;
      device_attribute >> string_vec;
      string_vec.pop_back();
      for (std::string i : string_vec) {
        bridged_attribute_value.data_StringArray.push_back((rust::string)i);
      }
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_ENUM:
  {

    // Get the labels
    Tango::AttributeInfoEx attr_info = device_proxy->get_attribute_config((std::string)attribute_name);
    for (std::string i : attr_info.enum_labels) {
      bridged_attribute_value.data_enum_labels.push_back((rust::string)i);
    }

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevEnum;
      Tango::DevShort enum_result;
      device_attribute >> enum_result;
      bridged_attribute_value.data_DevEnum = (int16_t)enum_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }
    break;
  }

  case Tango::DEV_STATE: {

    if (attr_data_format == Tango::AttrDataFormat::SCALAR)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevState;
      Tango::DevState state_result;
      device_attribute >> state_result;
      bridged_attribute_value.data_DevState = (DevState)state_result;
      bridged_attribute_data.data = bridged_attribute_value;
      return bridged_attribute_data;
    }

    if (attr_data_format == Tango::AttrDataFormat::SPECTRUM)
    {
      bridged_attribute_value.populated_command_arg_type = CmdArgType::DevVarStateArray;
        std::vector<Tango::DevState> state_result;
        device_attribute >> state_result;
        for (Tango::DevState i : state_result) {
          bridged_attribute_value.data_DevStateArray.push_back((DevState)i);
        }
        bridged_attribute_data.data = bridged_attribute_value;
        return bridged_attribute_data;
    }
    break;
  }

  default:
  {
    throw (std::string)"No match for result type for Tango enum id " + std::to_string(data_type);
  }

  }
  throw (std::string)"No conversion available";
}

rust::vec<BridgedAttributeInfo> BridgedDeviceProxy::attribute_list_query() const {

  Tango::AttributeInfoList attr_info_list;
  rust::vec<BridgedAttributeInfo> bridged_attr_info_list;
  attr_info_list = *device_proxy->attribute_list_query();
  for (Tango::AttributeInfo attr_info : attr_info_list) {
    BridgedAttributeInfo bridged_attr_info;
    switch (attr_info.data_format) {
    case Tango::AttrDataFormat::FMT_UNKNOWN:
      bridged_attr_info.data_format = AttrDataFormat::FMT_UNKNOWN;
      break;
    case Tango::AttrDataFormat::IMAGE:
      bridged_attr_info.data_format = AttrDataFormat::IMAGE;
      break;
    case Tango::AttrDataFormat::SCALAR:
      bridged_attr_info.data_format = AttrDataFormat::SCALAR;
      break;
    case Tango::AttrDataFormat::SPECTRUM:
      bridged_attr_info.data_format = AttrDataFormat::SPECTRUM;
      break;
    }
    bridged_attr_info.data_type = attr_info.data_type;
    bridged_attr_info.description = (rust::string)attr_info.description;
    switch (attr_info.disp_level) {
    case Tango::DispLevel::DL_UNKNOWN:
      bridged_attr_info.disp_level = DispLevel::DL_UNKNOWN;
      break;
    case Tango::DispLevel::EXPERT:
      bridged_attr_info.disp_level = DispLevel::EXPERT;
      break;
    case Tango::DispLevel::OPERATOR:
      bridged_attr_info.disp_level = DispLevel::OPERATOR;
      break;
    }
    bridged_attr_info.display_unit = (rust::string)attr_info.display_unit;
    bridged_attr_info.format = (rust::string)attr_info.format;
    bridged_attr_info.label = (rust::string)attr_info.label;
    bridged_attr_info.max_alarm = (rust::string)attr_info.max_alarm;
    bridged_attr_info.max_dim_x = attr_info.max_dim_x;
    bridged_attr_info.max_dim_y = attr_info.max_dim_y;
    bridged_attr_info.max_value = (rust::string)attr_info.max_value;
    bridged_attr_info.min_alarm = (rust::string)attr_info.min_alarm;
    bridged_attr_info.min_value = (rust::string)attr_info.min_value;
    bridged_attr_info.name = (rust::string)attr_info.name;
    bridged_attr_info.standard_unit = (rust::string)attr_info.standard_unit;
    bridged_attr_info.unit = (rust::string)attr_info.unit;
    switch (attr_info.writable) {
    case Tango::AttrWriteType::READ:
      bridged_attr_info.writable = AttrWriteType::READ;
      break;
    case Tango::AttrWriteType::WRITE:
      bridged_attr_info.writable = AttrWriteType::WRITE;
      break;
    case Tango::AttrWriteType::READ_WITH_WRITE:
      bridged_attr_info.writable = AttrWriteType::READ_WITH_WRITE;
      break;
    case Tango::AttrWriteType::READ_WRITE:
      bridged_attr_info.writable = AttrWriteType::READ_WRITE;
      break;
    case Tango::AttrWriteType::WT_UNKNOWN:
      bridged_attr_info.writable = AttrWriteType::WT_UNKNOWN;
      break;
    }
    bridged_attr_info.writable_attr_name = (rust::string)attr_info.writable_attr_name;
    bridged_attr_info_list.push_back(bridged_attr_info);
  }
  return bridged_attr_info_list;
}

rust::vec<BridgedCommandInfo> BridgedDeviceProxy::command_list_query() const {
  Tango::CommandInfoList command_info_list = *device_proxy->command_list_query();
  rust::vec<BridgedCommandInfo> command_info_vec;

  for (Tango::CommandInfo inf : command_info_list) {
    BridgedCommandInfo tmp_command_info;
    tmp_command_info.cmd_name = (rust::string)inf.cmd_name;
    tmp_command_info.cmd_tag = (int64_t)inf.cmd_tag;
    tmp_command_info.disp_level = (DispLevel)inf.cmd_tag;
    tmp_command_info.in_type = (CmdArgType)inf.in_type;
    tmp_command_info.in_type_desc = (rust::string)inf.in_type_desc;
    tmp_command_info.out_type = (CmdArgType)inf.out_type;
    tmp_command_info.out_type_desc = (rust::string)inf.out_type_desc;
    command_info_vec.push_back(tmp_command_info);
  }
  return command_info_vec;
}

std::unique_ptr<BridgedDeviceProxy> new_device_proxy(rust::str device_name) {
  return std::make_unique<BridgedDeviceProxy>(device_name);
}
