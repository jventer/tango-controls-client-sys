// CmdArgTypes =
// DeviceAttribute.get_type / DeviceData.get_type

// Adapated/copied from https://gitlab.com/tango-controls/tango-rs/-/blob/master/src/types.rs
use std::fmt::{self, Display};

use crate::bridge::{
    self,
    device_proxy_ffi::{BridgedAttributeInfo, BridgedCommandInfo},
};

pub type DevEncoded = (String, Vec<u8>);
pub type AttrWriteType = bridge::device_proxy_ffi::AttrWriteType;
pub type AttrDataFormat = bridge::device_proxy_ffi::AttrDataFormat;
pub type DispLevel = bridge::device_proxy_ffi::DispLevel;
pub type DevState = bridge::device_proxy_ffi::DevState;
pub type CmdArgType = bridge::device_proxy_ffi::CmdArgType;

#[derive(PartialEq, Debug, Clone, Default)]
pub struct DevAttrEnum {
    pub labels: Vec<String>,
    pub enum_value: u32,
}

impl Default for CmdArgType {
    fn default() -> Self {
        Self::DevVoid
    }
}

impl fmt::Display for CmdArgType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl fmt::Display for AttrWriteType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl fmt::Display for DispLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl fmt::Display for AttrDataFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl DevAttrEnum {
    fn get_label(&self) -> Option<&String> {
        return self.labels.get(self.enum_value as usize);
    }
}

impl Display for DevAttrEnum {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self.get_label() {
            Some(enum_val) => write!(f, "{}", enum_val),
            None => write!(f, "Cannot determine label"),
        }
    }
}

#[derive(PartialEq, Debug, Clone)]
pub enum AttrValue {
    Boolean(bool),
    BooleanArray(Vec<bool>),
    Short(i16),
    ShortArray(Vec<i16>),
    UShort(u16),
    UShortArray(Vec<u16>),
    Long(i32),
    LongArray(Vec<i32>),
    ULong(u32),
    ULongArray(Vec<u32>),
    Long64(i64),
    Long64Array(Vec<i64>),
    ULong64(u64),
    ULong64Array(Vec<u64>),
    Float(f32),
    FloatArray(Vec<f32>),
    Double(f64),
    DoubleArray(Vec<f64>),
    String(String),
    StringArray(Vec<String>),
    UChar(u8),
    UCharArray(Vec<u8>),
    DevState(DevState),
    DevStateArray(Vec<DevState>),
    DevEncoded(DevEncoded),
    DevEncodedArray(Vec<DevEncoded>),
    DevEnum(DevAttrEnum),
    DevEnumArray(Vec<DevAttrEnum>),
}

impl Default for AttrValue {
    fn default() -> Self {
        AttrValue::Boolean(true)
    }
}

impl AttrValue {
    /// Turn DevEncoded into a String
    /// ```
    /// use tango_controls_client_sys::types::{AttrValue, DevEncoded};
    /// let de = ("OO".to_string(), vec![1, 2, 3, 4]);
    /// assert_eq!(AttrValue::dev_encoded_display(&de), "[[OO], [1, 2, 3, 4]".to_string());
    /// ```
    #[allow(unused_assignments)]
    pub fn dev_encoded_display(dev_encoded: &DevEncoded) -> String {
        let mut display_str: String = String::new();
        display_str = format!("[[{}], [", dev_encoded.0);
        for (ucount, u_eight) in dev_encoded.1.iter().enumerate() {
            if ucount != 0 {
                display_str += ", ";
            }
            display_str += &format!("{}", u_eight);
        }
        display_str += "]";
        display_str
    }

    /// Turn Vector into a String
    /// ```
    /// use tango_controls_client_sys::types::AttrValue;
    /// let de = vec![1, 2, 3, 4];
    /// assert_eq!(AttrValue::vector_display(&de), "[1, 2, 3, 4]".to_string());
    ///
    /// let empty: Vec<u8> = vec![];
    /// assert_eq!(AttrValue::vector_display(&empty), "[]".to_string());
    /// ```
    pub fn vector_display<T: Display>(an_arr: &[T]) -> String {
        let mut display_str: String = "[".to_string();

        for (count, val) in an_arr.iter().enumerate() {
            if count != 0 {
                display_str += ", ";
            }
            display_str += &format!("{}", val);
        }
        display_str += "]";
        display_str
    }
}

impl fmt::Display for AttrValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Boolean(v) => fmt::Display::fmt(&v, f),
            Self::UChar(v) => fmt::Display::fmt(&v, f),
            Self::Short(v) => fmt::Display::fmt(&v, f),
            Self::UShort(v) => fmt::Display::fmt(&v, f),
            Self::Long(v) => fmt::Display::fmt(&v, f),
            Self::ULong(v) => fmt::Display::fmt(&v, f),
            Self::Long64(v) => fmt::Display::fmt(&v, f),
            Self::ULong64(v) => fmt::Display::fmt(&v, f),
            Self::Float(v) => fmt::Display::fmt(&v, f),
            Self::Double(v) => fmt::Display::fmt(&v, f),
            Self::String(v) => fmt::Display::fmt(&v, f),
            Self::DevState(v) => fmt::Display::fmt(&v, f),
            Self::DevEncoded(v) => {
                write!(f, "{}", AttrValue::dev_encoded_display(v))
            }
            Self::BooleanArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::UCharArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::ShortArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::UShortArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::LongArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::ULongArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::Long64Array(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::ULong64Array(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::FloatArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::DoubleArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::StringArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::DevStateArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
            Self::DevEncodedArray(v) => {
                write!(f, "[")?;
                for (count, da) in v.iter().enumerate() {
                    if count != 0 {
                        write!(f, ", ")?;
                    }
                    write!(f, "{}", AttrValue::dev_encoded_display(da))?;
                }
                write!(f, "]")
            }
            Self::DevEnum(v) => fmt::Display::fmt(&v, f),
            Self::DevEnumArray(v) => {
                write!(f, "{}", AttrValue::vector_display(v))
            }
        }
    }
}

#[derive(Debug, Clone)]
pub struct CommandInfo {
    pub cmd_name: String,
    pub cmd_tag: i64,
    pub in_type: CmdArgType,
    pub out_type: CmdArgType,
    pub in_type_desc: String,
    pub out_type_desc: String,
    pub disp_level: DispLevel,
}

impl From<BridgedCommandInfo> for CommandInfo {
    fn from(item: BridgedCommandInfo) -> Self {
        let display_level = item.disp_level;
        let in_type = item.in_type;
        let out_type = item.out_type;
        CommandInfo {
            cmd_name: item.cmd_name,
            cmd_tag: item.cmd_tag,
            in_type,
            out_type,
            in_type_desc: item.in_type_desc,
            out_type_desc: item.out_type_desc,
            disp_level: display_level,
        }
    }
}

#[derive(Debug, Clone)]
pub struct AttributeInfo {
    pub name: String,
    pub writable: AttrWriteType,
    pub data_type: i32,
    pub data_format: AttrDataFormat,
    pub max_dim_x: usize,
    pub max_dim_y: usize,
    pub description: String,
    pub label: String,
    pub unit: String,
    pub standard_unit: String,
    pub display_unit: String,
    pub format: String,
    pub min_value: String,
    pub max_value: String,
    pub min_alarm: String,
    pub max_alarm: String,
    pub writable_attr_name: String,
    pub disp_level: DispLevel,
}

impl Default for AttributeInfo {
    fn default() -> Self {
        Self {
            name: String::default(),
            writable: AttrWriteType::WT_UNKNOWN,
            data_type: 0,
            data_format: AttrDataFormat::FMT_UNKNOWN,
            max_dim_x: 0,
            max_dim_y: 0,
            description: String::default(),
            label: String::default(),
            unit: String::default(),
            standard_unit: String::default(),
            display_unit: String::default(),
            format: String::default(),
            min_value: String::default(),
            max_value: String::default(),
            min_alarm: String::default(),
            max_alarm: String::default(),
            writable_attr_name: String::default(),
            disp_level: DispLevel::DL_UNKNOWN,
        }
    }
}

impl From<&BridgedAttributeInfo> for AttributeInfo {
    fn from(item: &BridgedAttributeInfo) -> Self {
        AttributeInfo {
            name: item.name.clone(),
            writable: item.writable,
            data_type: item.data_type,
            data_format: item.data_format,
            max_dim_x: item.max_dim_x,
            max_dim_y: item.max_dim_y,
            description: item.description.clone(),
            label: item.label.clone(),
            unit: item.unit.clone(),
            standard_unit: item.standard_unit.clone(),
            display_unit: item.display_unit.clone(),
            format: item.format.clone(),
            min_value: item.min_value.clone(),
            max_value: item.max_value.clone(),
            min_alarm: item.min_alarm.clone(),
            max_alarm: item.max_alarm.clone(),
            writable_attr_name: item.writable_attr_name.clone(),
            disp_level: item.disp_level,
        }
    }
}

impl From<AttributeInfo> for CmdArgType {
    fn from(val: AttributeInfo) -> Self {
        match val.data_type {
            0 => CmdArgType::DevVoid,
            1 => CmdArgType::DevBoolean,
            2 => CmdArgType::DevShort,
            3 => CmdArgType::DevLong,
            4 => CmdArgType::DevFloat,
            5 => CmdArgType::DevDouble,
            6 => CmdArgType::DevUShort,
            7 => CmdArgType::DevULong,
            8 => CmdArgType::DevString,
            9 => CmdArgType::DevVarCharArray,
            10 => CmdArgType::DevVarShortArray,
            11 => CmdArgType::DevVarLongArray,
            12 => CmdArgType::DevVarFloatArray,
            13 => CmdArgType::DevVarDoubleArray,
            14 => CmdArgType::DevVarUShortArray,
            15 => CmdArgType::DevVarULongArray,
            16 => CmdArgType::DevVarStringArray,
            17 => CmdArgType::DevVarLongStringArray,
            18 => CmdArgType::DevVarDoubleStringArray,
            19 => CmdArgType::DevState,
            20 => CmdArgType::ConstDevString,
            21 => CmdArgType::DevVarBooleanArray,
            22 => CmdArgType::DevUChar,
            23 => CmdArgType::DevLong64,
            24 => CmdArgType::DevULong64,
            25 => CmdArgType::DevVarLong64Array,
            26 => CmdArgType::DevVarULong64Array,
            27 => CmdArgType::DevInt,
            28 => CmdArgType::DevEncoded,
            29 => CmdArgType::DevEnum,
            30 => CmdArgType::DevPipeBlob,
            31 => CmdArgType::DevVarStateArray,
            _ => CmdArgType::DataTypeUnknown,
        }
    }
}

pub type AttrQuality = bridge::device_proxy_ffi::AttrQuality;

#[derive(Debug, Clone, PartialEq)]
pub struct AttributeData {
    pub data: AttrValue,
    pub format: AttrDataFormat,
    pub quality: AttrQuality,
    pub name: String,
    pub dim_x: usize,
    pub dim_y: usize,
    pub time_stamp_tv_sec: i32,
    pub time_stamp_tv_usec: i32,
    pub time_stamp_tv_nsec: i32,
}

impl Default for AttributeData {
    fn default() -> Self {
        Self {
            data: Default::default(),
            format: AttrDataFormat::FMT_UNKNOWN,
            quality: AttrQuality::ATTR_VALID,
            name: Default::default(),
            dim_x: Default::default(),
            dim_y: Default::default(),
            time_stamp_tv_sec: Default::default(),
            time_stamp_tv_usec: Default::default(),
            time_stamp_tv_nsec: Default::default(),
        }
    }
}
