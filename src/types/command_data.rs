use crate::{bridge::device_proxy_ffi::DevState, types::DevEncoded};
use anyhow::{anyhow, Result};
use std::str::FromStr;

#[derive(PartialEq, Debug, Default, Clone)]
pub enum CommandData {
    #[default]
    Void,
    Boolean(bool),
    Short(i16),
    UShort(u16),
    Long(i32),
    ULong(u32),
    Long64(i64),
    ULong64(u64),
    DevState(DevState),
    DevEncoded(DevEncoded),
    Float(f32),
    Double(f64),
    String(String),
    Char(u8),
    BooleanArray(Vec<bool>),
    CharArray(Vec<u8>),
    ShortArray(Vec<i16>),
    UShortArray(Vec<u16>),
    LongArray(Vec<i32>),
    ULongArray(Vec<u32>),
    Long64Array(Vec<i64>),
    ULong64Array(Vec<u64>),
    FloatArray(Vec<f32>),
    DoubleArray(Vec<f64>),
    StringArray(Vec<String>),
    LongStringArray(Vec<i32>, Vec<String>),
    DoubleStringArray(Vec<f64>, Vec<String>),
}

//     Boolean(bool),
impl TryFrom<CommandData> for bool {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Boolean(val) => Ok(val),
            _ => Err(anyhow!("No conversion available bool")),
        }
    }
}
impl From<bool> for CommandData {
    fn from(item: bool) -> Self {
        CommandData::Boolean(item)
    }
}

//     Short(u16),
impl TryFrom<CommandData> for i16 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Short(val) => Ok(val),
            _ => Err(anyhow!("No conversion available i16")),
        }
    }
}
impl From<i16> for CommandData {
    fn from(item: i16) -> Self {
        CommandData::Short(item)
    }
}

//     UShort(u16),
impl TryFrom<CommandData> for u16 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::UShort(val) => Ok(val),
            _ => Err(anyhow!("No conversion available u16")),
        }
    }
}
impl From<u16> for CommandData {
    fn from(item: u16) -> Self {
        CommandData::UShort(item)
    }
}

//     Long(i32),
impl TryFrom<CommandData> for i32 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Long(val) => Ok(val),
            _ => Err(anyhow!("No conversion available i32")),
        }
    }
}
impl From<i32> for CommandData {
    fn from(item: i32) -> Self {
        CommandData::Long(item)
    }
}

//     ULong(u32)
impl TryFrom<CommandData> for u32 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::ULong(val) => Ok(val),
            _ => Err(anyhow!("No conversion available u32")),
        }
    }
}
impl From<u32> for CommandData {
    fn from(item: u32) -> Self {
        CommandData::ULong(item)
    }
}

//     Long64(i64),
impl TryFrom<CommandData> for i64 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Long64(val) => Ok(val),
            _ => Err(anyhow!("No conversion available i64")),
        }
    }
}
impl From<i64> for CommandData {
    fn from(item: i64) -> Self {
        CommandData::Long64(item)
    }
}

//     ULong64(u64),
impl TryFrom<CommandData> for u64 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::ULong64(val) => Ok(val),
            _ => Err(anyhow!("No conversion available u64")),
        }
    }
}
impl From<u64> for CommandData {
    fn from(item: u64) -> Self {
        CommandData::ULong64(item)
    }
}

//     DevState(DevState),
impl TryFrom<CommandData> for DevState {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::DevState(val) => Ok(val),
            _ => Err(anyhow!("No conversion available DevState")),
        }
    }
}
impl From<DevState> for CommandData {
    fn from(item: DevState) -> Self {
        CommandData::DevState(item)
    }
}

//     DevEncoded(DevEncoded),
impl TryFrom<CommandData> for DevEncoded {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::DevEncoded(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<DevEncoded> for CommandData {
    fn from(item: DevEncoded) -> Self {
        CommandData::DevEncoded(item)
    }
}

//     Float(f32),
impl TryFrom<CommandData> for f32 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Float(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<f32> for CommandData {
    fn from(item: f32) -> Self {
        CommandData::Float(item)
    }
}

//     Double(f64),
impl TryFrom<CommandData> for f64 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Double(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<f64> for CommandData {
    fn from(item: f64) -> Self {
        CommandData::Double(item)
    }
}

//     String(String),
// impl TryFrom<CommandData> for String {
//     type Error = anyhow::Error;

//     fn try_from(item: CommandData) -> Result<Self, Self::Error> {
//         match item {
//             CommandData::String(val) => Ok(val),
//             _ => Err(anyhow!("No conversion available")),
//         }
//     }
// }
// impl From<String> for CommandData {
//     fn from(item: String) -> Self {
//         CommandData::String(item)
//     }
// }

impl FromStr for CommandData {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(CommandData::String(s.to_string()))
    }
}

//     UChar(u8),
impl TryFrom<CommandData> for u8 {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Char(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<u8> for CommandData {
    fn from(item: u8) -> Self {
        CommandData::Char(item)
    }
}

//     BooleanArray(Vec<bool>),
// impl TryFrom<CommandData> for Vec<bool> {
//     type Error = anyhow::Error;

//     fn try_from(item: CommandData) -> Result<Self, Self::Error> {
//         match item {
//             CommandData::BooleanArray(val) => Ok(val),
//             _ => Err(anyhow!("No conversion available")),
//         }
//     }
// }
// impl From<Vec<bool>> for CommandData {
//     fn from(item: Vec<bool>) -> Self {
//         CommandData::BooleanArray(item)
//     }
// }

//     CharArray(Vec<u8>),
impl TryFrom<CommandData> for Vec<u8> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::CharArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<u8>> for CommandData {
    fn from(item: Vec<u8>) -> Self {
        CommandData::CharArray(item)
    }
}

//     StringArray(Vec<String>),
impl TryFrom<CommandData> for Vec<String> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::StringArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<String>> for CommandData {
    fn from(item: Vec<String>) -> Self {
        CommandData::StringArray(item)
    }
}

//     ShortArray(Vec<i16>),
impl TryFrom<CommandData> for Vec<i16> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::ShortArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<i16>> for CommandData {
    fn from(item: Vec<i16>) -> Self {
        CommandData::ShortArray(item)
    }
}

//     UShortArray(Vec<u16>),
impl TryFrom<CommandData> for Vec<u16> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::UShortArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<u16>> for CommandData {
    fn from(item: Vec<u16>) -> Self {
        CommandData::UShortArray(item)
    }
}

//     LongArray(Vec<i32>),
impl TryFrom<CommandData> for Vec<i32> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::LongArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<i32>> for CommandData {
    fn from(item: Vec<i32>) -> Self {
        CommandData::LongArray(item)
    }
}

//     ULongArray(Vec<u32>),
impl TryFrom<CommandData> for Vec<u32> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::ULongArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<u32>> for CommandData {
    fn from(item: Vec<u32>) -> Self {
        CommandData::ULongArray(item)
    }
}

//     Long64Array(Vec<i64>),
impl TryFrom<CommandData> for Vec<i64> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::Long64Array(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<i64>> for CommandData {
    fn from(item: Vec<i64>) -> Self {
        CommandData::Long64Array(item)
    }
}

//     ULong64Array(Vec<u64>),
impl TryFrom<CommandData> for Vec<u64> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::ULong64Array(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<u64>> for CommandData {
    fn from(item: Vec<u64>) -> Self {
        CommandData::ULong64Array(item)
    }
}

//     FloatArray(Vec<f32>),
impl TryFrom<CommandData> for Vec<f32> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::FloatArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<f32>> for CommandData {
    fn from(item: Vec<f32>) -> Self {
        CommandData::FloatArray(item)
    }
}

//     DoubleArray(Vec<f64>),
impl TryFrom<CommandData> for Vec<f64> {
    type Error = anyhow::Error;

    fn try_from(item: CommandData) -> Result<Self, Self::Error> {
        match item {
            CommandData::DoubleArray(val) => Ok(val),
            _ => Err(anyhow!("No conversion available")),
        }
    }
}
impl From<Vec<f64>> for CommandData {
    fn from(item: Vec<f64>) -> Self {
        CommandData::DoubleArray(item)
    }
}
