#include <tango.h>
#include "rust/cxx.h"
#include <tango-controls-client-sys/src/bridge.rs.h>

class BridgedDatabase::impl {
  friend BridgedDatabase;
  Tango::Database* database = 0;
};

BridgedDatabase::BridgedDatabase() : impl(new class BridgedDatabase::impl) {
  database = new Tango::Database();
}

BridgedDatabase::~BridgedDatabase() {
  if (database) {
    delete database;
  }
}

rust::vec<rust::string> BridgedDatabase::get_device_exported(rust::Str filter) const {
  Tango::DbDatum db_datum = database->get_device_exported_for_class((std::string)filter);
  std::vector<std::string> dev_list;
  db_datum >> dev_list;
  rust::vec<rust::string> result;
  for (std::string i : dev_list) {
    result.push_back((rust::string)i);
  }
  return result;
}


std::unique_ptr<BridgedDatabase> new_database() {
  return std::make_unique<BridgedDatabase>();
}