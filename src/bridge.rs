use std::fmt;

use crate::types::{CmdArgType, DevState};

use self::device_proxy_ffi::BridgedBool;

#[cxx::bridge]
pub mod device_proxy_ffi {
    // Only unit enums are supported

    #[derive(PartialEq, Debug, Clone)]
    pub enum CmdArgType {
        DevVoid,
        DevBoolean,
        DevShort,
        DevLong,
        DevFloat,
        DevDouble,
        DevUShort,
        DevULong,
        DevString,
        DevVarCharArray,
        DevVarShortArray,
        DevVarLongArray,
        DevVarFloatArray,
        DevVarDoubleArray,
        DevVarUShortArray,
        DevVarULongArray,
        DevVarStringArray,
        DevVarLongStringArray,
        DevVarDoubleStringArray,
        DevState,
        ConstDevString,
        DevVarBooleanArray,
        DevUChar,
        DevLong64,
        DevULong64,
        DevVarLong64Array,
        DevVarULong64Array,
        DevInt,
        DevEncoded,
        DevEnum,
        DevPipeBlob,
        DevVarStateArray,
        DataTypeUnknown = 100,
    }

    #[derive(Debug, PartialEq, Copy)]
    pub enum DevState {
        ON,
        OFF,
        CLOSE,
        OPEN,
        INSERT,
        EXTRACT,
        MOVING,
        STANDBY,
        FAULT,
        INIT,
        RUNNING,
        ALARM,
        DISABLE,
        UNKNOWN,
    }

    #[derive(Debug, PartialEq, Hash, Ord, PartialOrd)]
    pub enum BridgedBool {
        BridgedTrue,
        BridgedFalse,
    }

    #[derive(Debug, PartialEq)]
    pub struct BridgedCommandInfo {
        cmd_name: String,
        cmd_tag: i64,
        in_type: CmdArgType,
        out_type: CmdArgType,
        in_type_desc: String,
        out_type_desc: String,
        disp_level: DispLevel,
    }

    #[derive(PartialEq, Clone, Debug)]
    pub struct BridgedCmdArgTypeData {
        // populated_device_data is used to determine which of these struct values has the data
        // NOT SUPPORTED :
        //  ConstDevString,
        //  DevPipeBlob,
        //  DevVarLongStringArray
        //  DevVarDoubleStringArray
        //  DataTypeUnknown = 100,
        populated_command_arg_type: CmdArgType,
        data_Void: bool,                     // DevVoid
        data_Boolean: BridgedBool,           // DevBoolean
        data_BooleanArray: Vec<BridgedBool>, // DevVarBooleanArray
        data_Short: i16,                     // DevShort
        data_ShortArray: Vec<i16>,           // DevVarShortArray
        data_Long: i32,                      // DevLong
        data_LongArray: Vec<i32>,            // DevVarLongArray
        data_Float: f32,                     // DevFloat
        data_FloatArray: Vec<f32>,           // DevVarFloatArray
        data_Double: f64,                    // Double
        data_DoubleArray: Vec<f64>,          // DevVarDoubleArray
        data_UShort: u16,                    // DevUShort
        data_UShortArray: Vec<u16>,          // DevVarUShortArray
        data_ULong: u32,                     // DevULong
        data_Int: u32,                       // DevInt
        data_ULongArray: Vec<u32>,           //DevVarULongArray
        data_String: String,                 // DevString
        data_StringArray: Vec<String>,       // DevVarStringArray
        data_UChar: u8,                      // DevUChar
        data_CharArray: Vec<u8>,             // DevVarCharArray,
        data_DevState: DevState,             // DevState, DevVarStateArray
        data_DevStateArray: Vec<DevState>,   // DevState, DevVarStateArray

        data_Long64: i64,           // DevLong64
        data_Long64Array: Vec<i64>, // DevVarLong64Array

        // DevEncoded
        // Combine data_String and data_CharArray for (String, Vec<u8>)
        data_DevEncoded: bool,

        // DevVarLongStringArray
        // Combine data_FloatArray and data_StringArray for Vec<(f32, String)>
        data_LongStringArray: bool,

        // DevVarDoubleStringArray
        // Combine data_DoubleArray and data_StringArray for Vec<(f64, String)>
        data_DoubleStringArray: bool,

        data_ULong64: u64,             // DevULong64
        data_ULong64Array: Vec<u64>,   // DevVarULong64Array
        data_DevEnum: u32,             // DevEnum
        data_enum_labels: Vec<String>, // Extra store for enum labels
    }

    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub enum AttrDataFormat {
        SCALAR,
        SPECTRUM,
        IMAGE,
        FMT_UNKNOWN,
    }

    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub enum AttrQuality {
        ATTR_VALID,
        ATTR_INVALID,
        ATTR_ALARM,
        ATTR_CHANGING,
        ATTR_WARNING,
    }

    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub enum AttrWriteType {
        READ,
        READ_WITH_WRITE,
        WRITE,
        READ_WRITE,
        WT_UNKNOWN,
    }

    #[derive(Debug, PartialEq, Eq, Clone, Copy)]
    pub enum DispLevel {
        OPERATOR,
        EXPERT,
        DL_UNKNOWN,
    }

    #[derive(Debug)]
    pub struct BridgedAttributeInfo {
        pub name: String,
        pub writable: AttrWriteType,
        pub data_type: i32,
        pub data_format: AttrDataFormat,
        pub max_dim_x: usize,
        pub max_dim_y: usize,
        pub description: String,
        pub label: String,
        pub unit: String,
        pub standard_unit: String,
        pub display_unit: String,
        pub format: String,
        pub min_value: String,
        pub max_value: String,
        pub min_alarm: String,
        pub max_alarm: String,
        pub writable_attr_name: String,
        pub disp_level: DispLevel,
    }

    #[derive(Debug, Clone, PartialEq)]
    pub struct BridgedAttributeData {
        pub data: BridgedCmdArgTypeData,
        pub format: AttrDataFormat,
        pub quality: AttrQuality,
        pub name: String,
        pub dim_x: usize,
        pub dim_y: usize,
        pub time_stamp_tv_sec: i32,
        pub time_stamp_tv_usec: i32,
        pub time_stamp_tv_nsec: i32,
    }

    unsafe extern "C++" {
        include!("tango-controls-client-sys/src/include/catcher.h");
        include!("tango-controls-client-sys/src/include/device_proxy.h");

        type BridgedDeviceProxy;
        // type CmdArgType;

        fn new_device_proxy(device_name: &str) -> Result<UniquePtr<BridgedDeviceProxy>>;
        fn ping(&self) -> Result<i32>;
        fn state(&self) -> Result<DevState>;
        fn status(&self) -> Result<String>;
        fn command_query(&self, command_name: &str) -> Result<BridgedCommandInfo>;
        fn command_inout(
            &self,
            command_name: &str,
            bridged_command_data: BridgedCmdArgTypeData,
        ) -> Result<BridgedCmdArgTypeData>;
        fn get_command_list(&self) -> Result<Vec<String>>;
        fn get_attribute_list(&self) -> Result<Vec<String>>;
        fn read_attribute(&self, attribute_name: &str) -> Result<BridgedAttributeData>;
        fn attribute_list_query(&self) -> Result<Vec<BridgedAttributeInfo>>;
        fn command_list_query(&self) -> Result<Vec<BridgedCommandInfo>>;
    }
}

impl Default for device_proxy_ffi::DevState {
    fn default() -> Self {
        device_proxy_ffi::DevState::UNKNOWN
    }
}

impl std::fmt::Display for device_proxy_ffi::DevState {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

impl Default for device_proxy_ffi::BridgedCmdArgTypeData {
    fn default() -> Self {
        device_proxy_ffi::BridgedCmdArgTypeData {
            populated_command_arg_type: CmdArgType::DataTypeUnknown,
            data_Void: true,
            data_Boolean: BridgedBool::BridgedFalse,
            data_BooleanArray: Vec::new(),
            data_Short: 0,
            data_ShortArray: Vec::new(),
            data_Long: 0,
            data_LongArray: Vec::new(),
            data_Float: 0.0,
            data_FloatArray: Vec::new(),
            data_Double: 0.0,
            data_DoubleArray: Vec::new(),
            data_UShort: 0,
            data_UShortArray: Vec::new(),
            data_ULong: 0,
            data_Int: 0,
            data_ULongArray: Vec::new(),
            data_String: "".to_string(),
            data_StringArray: Vec::new(),
            data_UChar: 0,
            data_CharArray: Vec::new(),
            data_DevState: DevState::UNKNOWN,
            data_DevStateArray: Vec::new(),
            data_Long64: 0,
            data_Long64Array: Vec::new(),
            data_DevEncoded: false, // Combine data_String and data_CharArray for Vec<(String, Vec<u8>)>
            data_ULong64: 0,
            data_ULong64Array: Vec::new(),
            data_DevEnum: 0,
            data_enum_labels: Vec::new(),
            data_DoubleStringArray: false, // Combine data_DoubleArray and data_StringArray for Vec<(f64, String)>
            data_LongStringArray: false, // Combine data_FloatArray and data_StringArray for Vec<(f32, String)>
        }
    }
}

#[cxx::bridge]
pub mod database_ffi {

    unsafe extern "C++" {
        include!("tango-controls-client-sys/src/include/catcher.h");
        include!("tango-controls-client-sys/src/include/database_proxy.h");

        type BridgedDatabase;
        fn new_database() -> Result<UniquePtr<BridgedDatabase>>;
        fn get_device_exported(&self, filter: &str) -> Result<Vec<String>>;
    }
}
