#[cfg(test)]
mod test_dp {
    use std::str::FromStr;

    use crate::device_proxy;
    use crate::types::{
        AttrDataFormat, AttrQuality, AttrValue, AttrWriteType, CmdArgType, CommandData,
        DevAttrEnum, DevState, DispLevel,
    };

    #[test]
    fn test_connection_ok() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(_) => assert!(true),
            Err(_) => assert!(false, "Test device should be running"),
        }
    }

    #[test]
    fn test_connection_fail() {
        let dp_res = device_proxy::DeviceProxy::new("a/b/c");
        match dp_res {
            Ok(_) => assert!(false, "DeviceProxy should not exist"),
            Err(err) => assert_eq!(err.to_string(), "DB_DeviceNotDefined"),
        }
    }

    #[test]
    fn test_ping() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(dp) => {
                let ping_result = dp.ping();
                match ping_result {
                    Ok(res) => assert!(res >= 0),
                    Err(err) => assert!(false, "There should be no error, got {:?}", err),
                }
            }
            Err(err) => assert!(false, "There should be no error, got {:?}", err),
        }
    }

    #[test]
    fn test_dev_name() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(dp) => {
                assert_eq!(dp.device_name, "sys/tg_test/1")
            }
            Err(err) => assert!(false, "There should be no error, got {:?}", err),
        }
    }

    #[test]
    fn test_state() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(dp) => {
                let state_result = dp.state();
                match state_result {
                    Ok(state) => assert_eq!(state, DevState::RUNNING),
                    Err(err) => assert!(false, "There should be no error, got {:?}", err),
                }
            }
            Err(err) => assert!(false, "There should be no error, got {:?}", err),
        }
    }

    #[test]
    fn test_status() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(dp) => {
                let status_result = dp.status();
                match status_result {
                    Ok(status) => assert_eq!(status, "The device is in RUNNING state.".to_string()),
                    Err(err) => assert!(false, "There should be no error, got {:?}", err),
                }
            }
            Err(err) => assert!(false, "There should be no error, got {:?}", err),
        }
    }

    #[test]
    fn test_command_info() {
        let dp_res = device_proxy::DeviceProxy::new("sys/tg_test/1");
        match dp_res {
            Ok(dp) => {
                let query_result = dp.command_query("State");
                match query_result {
                    Ok(command) => {
                        assert_eq!(command.cmd_name, "State".to_string());
                        assert_eq!(command.in_type_desc, "Uninitialised".to_string());
                        assert_eq!(command.out_type_desc, "Device state".to_string());
                        assert_eq!(command.in_type, CmdArgType::DevVoid);
                        assert_eq!(command.out_type, CmdArgType::DevState);
                        assert_eq!(command.disp_level, DispLevel::OPERATOR);
                    }
                    Err(err) => assert!(false, "There should be no error, got {:?}", err),
                }
            }
            Err(err) => assert!(false, "There should be no error, got {:?}", err),
        }
    }

    #[test]
    fn test_commands() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");

        // test all types
        println!("\nTesting commands for all data types:");
        let tests = vec![
            ("DevVoid", CommandData::Void),
            ("DevBoolean", CommandData::Boolean(true)),
            ("DevShort", CommandData::Short(-147)),
            ("DevLong", CommandData::Long(-(1 << 20))),
            ("DevFloat", CommandData::Float(42.42)),
            ("DevDouble", CommandData::Double(123.456790123752)),
            ("DevUShort", CommandData::UShort(137)),
            ("DevULong", CommandData::ULong(1 << 20)),
            ("DevLong64", CommandData::Long64(-(1 << 60))),
            ("DevULong64", CommandData::ULong64(1 << 60)),
            (
                "DevString",
                CommandData::from_str("test_string").expect("Could not convert string"),
            ),
            ("DevVarCharArray", CommandData::CharArray(vec![1, 5, 7])),
            (
                "DevVarStringArray",
                CommandData::StringArray(vec!["AAA".to_string(), "BBB".to_string()]),
            ),
            ("DevVarShortArray", CommandData::ShortArray(vec![-5, 1, 0])),
            ("DevVarUShortArray", CommandData::UShortArray(vec![5, 1, 0])),
            (
                "DevVarLongArray",
                CommandData::LongArray(vec![-(1 << 20), 1, 0]),
            ),
            (
                "DevVarULongArray",
                CommandData::ULongArray(vec![1 << 30, 1, 0]),
            ),
            (
                "DevVarLong64Array",
                CommandData::Long64Array(vec![-(1 << 60), 1, 0]),
            ),
            (
                "DevVarULong64Array",
                CommandData::ULong64Array(vec![1 << 60, 1, 0]),
            ),
            (
                "DevVarFloatArray",
                CommandData::FloatArray(vec![-42.4, 0.0, 80.123]),
            ),
            (
                "DevVarDoubleArray",
                CommandData::DoubleArray(vec![-5.0, 1.0, 0.0]),
            ),
            (
                "DevVarLongStringArray",
                CommandData::LongStringArray(
                    vec![-(1 << 20), 1, -2],
                    vec!["CCC".to_string(), "DDD".to_string()],
                ),
            ),
            (
                "DevVarDoubleStringArray",
                CommandData::DoubleStringArray(
                    vec![-5.0, 1.0, 0.0],
                    vec!["EEE".to_string(), "FFF".to_string()],
                ),
            ),
            ("State", CommandData::DevState(DevState::RUNNING)),
            (
                "DevVarLongStringArray",
                CommandData::LongStringArray(vec![1, 2, 3], vec!["A".to_string(), "B".to_string()]),
            ),
            (
                "DevVarDoubleStringArray",
                CommandData::DoubleStringArray(
                    vec![1.0, 2.0, 3.0],
                    vec!["A".to_string(), "B".to_string()],
                ),
            ),
        ];
        for (cmd, data) in tests {
            println!("Testing command {}", cmd);
            let res = dp
                .command_inout(cmd, data.clone())
                .expect("Could not execute command on sys/tg_test/1");
            assert_eq!(data, res);
        }
    }

    #[test]
    fn test_command_list() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not connect to sys/tg_test/1");
        let command_list: Vec<String> = dp
            .get_command_list()
            .expect("Call to get_command_list failed");
        assert_eq!(command_list.len(), 30);
        assert_eq!(command_list[0], "CrashFromDevelopperThread".to_string());
        assert_eq!(command_list[15], "DevVarFloatArray".to_string());
        assert_eq!(command_list[29], "SwitchStates".to_string());
    }

    #[test]
    fn test_attribute_list() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not connect to sys/tg_test/1");
        let mut attribute_list: Vec<String> = dp
            .get_attribute_list()
            .expect("Call to get_command_list failed");
        attribute_list.sort();
        assert_eq!(attribute_list.len(), 64);
        assert_eq!(attribute_list[2], "ampli".to_string());
        assert_eq!(attribute_list[39], "short_spectrum".to_string());
        assert_eq!(attribute_list[1], "Status".to_string());
    }

    #[test]
    fn test_attr_read() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");

        // test all types
        println!("\nTesting attribute reads:");
        let read_tests = vec![
            "boolean_scalar",
            "boolean_spectrum",
            "uchar_scalar",
            "uchar_spectrum",
            "short_scalar",
            "short_spectrum",
            "ushort_scalar",
            "ushort_spectrum",
            "long_scalar",
            "long_spectrum",
            "ulong_scalar",
            "ulong_spectrum_ro",
            "long64_scalar",
            "long64_spectrum_ro",
            "ulong64_scalar",
            "ulong64_spectrum_ro",
            "float_scalar",
            "float_spectrum",
            "double_scalar",
            "double_spectrum",
            "string_scalar",
            "string_spectrum",
            "enum_scalar",
            "State",
        ];
        for attr_name in read_tests {
            println!("Testing attribute {}", attr_name);
            let _res = dp
                .read_attribute(attr_name)
                .expect("Could not read attribute on sys/tg_test/1");
        }
    }

    macro_rules! attr_read_tests {
        ($($name:ident: $value:expr,)*) => {
        $(
            #[test]
            fn $name() {
                let (attr_name, attr_value_option, attr_len) = $value;

                let dp = device_proxy::DeviceProxy::new("sys/tg_test/1").expect("Could not to connect to sys/tg_test/1");
                let res = dp.read_attribute(attr_name).expect("Could not read attribute on sys/tg_test/1");

                if let Some(attr_value) = attr_value_option
                {
                    assert_eq!(res.data, attr_value);
                }

                match res.data {
                    AttrValue::BooleanArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::ShortArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::UShortArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::LongArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::ULongArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::Long64Array(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::ULong64Array(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::FloatArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::DoubleArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::StringArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::UCharArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::DevStateArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::DevEncodedArray(val) => assert_eq!(val.len(), attr_len),
                    AttrValue::DevEnumArray(val) => assert_eq!(val.len(), attr_len),
                    _ => (),
                }
            }
        )*
        }
    }

    attr_read_tests! {
        boolean_scalar: ("boolean_scalar", Some(AttrValue::Boolean(true)), 0),
        boolean_spectrum: ("boolean_spectrum", Some(AttrValue::BooleanArray(vec![false;256])), 256),
        uchar_scalar: ("uchar_scalar", Some(AttrValue::UChar(0)), 0),
        uchar_spectrum: ("uchar_spectrum", Some(AttrValue::UCharArray(vec![0;256])), 256),
        short_spectrum: ("short_spectrum", Some(AttrValue::ShortArray(vec![0;256])), 256),
        ushort_scalar: ("ushort_scalar", Some(AttrValue::UShort(0)), 0),
        ushort_spectrum: ("ushort_spectrum", Some(AttrValue::UShortArray(vec![0;256])), 256),
        long_spectrum: ("long_spectrum", Some(AttrValue::LongArray(vec![0;256])), 256),
        float_scalar: ("float_scalar", Some(AttrValue::Float(0.0)), 0),
        float_spectrum: ("float_spectrum", Some(AttrValue::FloatArray(vec![0.0;256])), 256),
        double_spectrum: ("double_spectrum", Some(AttrValue::DoubleArray(vec![0.0;256])), 256),
        string_scalar: ("string_scalar", Some(AttrValue::String("Default string".to_string())), 0),
        string_spectrum: ("string_spectrum", Some(AttrValue::StringArray(vec!["".to_string();256])), 256),
        enum_scalar: ("enum_scalar", Some(AttrValue::DevEnum(DevAttrEnum{ labels: vec!["LABEL0".to_string(), "LABEL1".to_string(), "LABEL2".to_string()], enum_value:0})), 0),
        // These are all random, can't check values
        double_scalar: ("double_scalar", None, 256),
        long64_scalar: ("long64_scalar", None, 256),
        long64_spectrum_ro: ("long64_spectrum_ro", None, 256),
        ulong64_scalar: ("ulong64_scalar", None, 256),
        ulong64_spectrum_ro: ("ulong64_spectrum_ro", None, 256),
        long_scalar: ("long_scalar", None, 256),
        short_scalar: ("short_scalar", None, 256),
        ulong_scalar: ("ulong_scalar", None, 256),
        ulong_spectrum_ro: ("ulong_spectrum_ro", None, 256),
    }

    #[test]
    fn test_enum_labels() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");

        let res = dp
            .read_attribute("enum_scalar")
            .expect("Could not read attribute on sys/tg_test/1");

        assert_eq!(res.dim_x, 1);
        assert_eq!(res.dim_y, 0);
        assert_eq!(res.format, AttrDataFormat::SCALAR);
        assert_eq!(res.name, "enum_scalar".to_string());
        assert_eq!(res.quality, AttrQuality::ATTR_VALID);
        assert!(res.time_stamp_tv_sec > 1667841290);
        assert!(res.time_stamp_tv_nsec >= 0);
        assert!(res.time_stamp_tv_usec >= 0);

        match res.data {
            AttrValue::DevEnum(val) => {
                assert_eq!(val.labels, vec!["LABEL0", "LABEL1", "LABEL2"]);
                let v: Vec<u32> = vec![0, 1, 2];
                assert!(v.contains(&val.enum_value));
            }
            _ => {
                assert!(false, "Incorrect match");
            }
        }
    }

    #[test]
    fn test_non_existant_attr_read() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");

        let res = dp.read_attribute("does_not_exist");

        match res {
            Ok(_) => assert!(false, "An error is expected"),
            Err(err) => assert_eq!(
                "does_not_exist attribute not found".to_string(),
                err.to_string()
            ),
        }
    }

    #[test]
    fn test_image_not_supported() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");

        let res = dp.read_attribute("string_image");

        match res {
            Ok(_) => assert!(false, "An error is expected"),
            Err(err) => assert_eq!(
                "tango.AttrDataFormat.IMAGE not supported".to_string(),
                err.to_string()
            ),
        }
    }

    #[test]
    fn test_attribute_list_query() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");
        let attributes_info = dp
            .attribute_list_query()
            .expect("attribute_list_query command failed");
        assert_eq!(attributes_info.len(), 64);
        let attr_info = &attributes_info[0];
        assert_eq!(attr_info.name, "ampli".to_string());
        assert_eq!(attr_info.writable, AttrWriteType::WRITE);
        assert_eq!(attr_info.data_type, 5);
        assert_eq!(attr_info.data_format, AttrDataFormat::SCALAR);
        assert_eq!(attr_info.max_dim_x, 1);
        assert_eq!(attr_info.max_dim_y, 0);
        assert_eq!(attr_info.description, "No description".to_string());
        assert_eq!(attr_info.label, "ampli".to_string());
        assert_eq!(attr_info.unit, "".to_string());
        assert_eq!(attr_info.standard_unit, "No standard unit".to_string());
        assert_eq!(attr_info.display_unit, "No display unit".to_string());
        assert_eq!(attr_info.format, "%6.2f".to_string());
        assert_eq!(attr_info.min_value, "Not specified".to_string());
        assert_eq!(attr_info.max_value, "Not specified".to_string());
        assert_eq!(attr_info.min_alarm, "Not specified".to_string());
        assert_eq!(attr_info.max_alarm, "Not specified".to_string());
        assert_eq!(attr_info.writable_attr_name, "None".to_string());
        assert_eq!(attr_info.disp_level, DispLevel::OPERATOR);
    }

    #[test]
    fn test_command_list_query() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");
        let command_list = dp
            .command_list_query()
            .expect("attribute_list_query command failed");
        assert_eq!(command_list.len(), 30);
        let comm_info = &command_list[0];
        assert_eq!(comm_info.cmd_name, "CrashFromDevelopperThread".to_string());
        assert_eq!(comm_info.cmd_tag, 0);
        assert_eq!(comm_info.in_type, CmdArgType::DevVoid);
        assert_eq!(comm_info.out_type, CmdArgType::DevVoid);
        assert_eq!(comm_info.in_type_desc, "Uninitialised".to_string());
        assert_eq!(comm_info.out_type_desc, "Uninitialised".to_string());
        assert_eq!(comm_info.disp_level, DispLevel::OPERATOR);
    }

    #[test]
    fn test_display() {
        let att = AttrValue::Boolean(true);
        assert_eq!(att.to_string(), "true".to_string());
        let att_arr = AttrValue::BooleanArray(vec![true, false, true]);
        assert_eq!(att_arr.to_string(), "[true, false, true]".to_string());
        let ds = DevState::ALARM;
        assert_eq!(ds.to_string(), "ALARM".to_string());
        let ds_arr = vec![DevState::ALARM, DevState::OFF];
        assert_eq!(format!("{:?}", ds_arr), "[ALARM, OFF]".to_string());
        let dva: AttrValue = AttrValue::DevEnumArray(vec![
            DevAttrEnum {
                labels: vec!["AA".to_string(), "BB".to_string()],
                enum_value: 1,
            },
            DevAttrEnum {
                labels: vec!["CC".to_string(), "DD".to_string()],
                enum_value: 0,
            },
        ]);
        assert_eq!(format!("{}", dva), "[BB, CC]".to_string());
        let de: AttrValue = AttrValue::DevEncoded(("OO".to_string(), vec![1, 2, 3, 4]));
        assert_eq!(format!("{}", de), "[[OO], [1, 2, 3, 4]".to_string());
        let dev: AttrValue = AttrValue::DevEncodedArray(vec![
            ("OO".to_string(), vec![1, 2, 3, 4]),
            ("PP".to_string(), vec![5, 6]),
        ]);
        assert_eq!(
            dev.to_string(),
            "[[[OO], [1, 2, 3, 4], [[PP], [5, 6]]".to_string()
        );
        let state: AttrValue = AttrValue::DevState(DevState::MOVING);
        assert_eq!(state.to_string(), "MOVING".to_string());
    }

    #[test]
    fn test_state_status() {
        let dp = device_proxy::DeviceProxy::new("sys/tg_test/1")
            .expect("Could not to connect to sys/tg_test/1");
        let res = dp
            .command_inout("State", CommandData::Void)
            .expect("Could not run State");
        match res {
            CommandData::DevState(s) => assert_eq!(s.to_string(), "RUNNING".to_string()),
            _ => assert!(false, "Should not get anything else"),
        }
        let res = dp
            .command_inout("Status", CommandData::Void)
            .expect("Could not run Status");
        match res {
            CommandData::String(s) => {
                assert_eq!(s, "The device is in RUNNING state.".to_string())
            }
            _ => assert!(false, "Should not get anything else"),
        }
    }
}

#[cfg(test)]
mod test_db {

    use crate::database_proxy::{self};

    #[test]
    fn test_connection_db() {
        let db_res = database_proxy::DatabaseProxy::new();
        match db_res {
            Ok(_) => assert!(true),
            Err(_) => assert!(false, "Database creation failed"),
        }
    }

    #[test]
    fn test_exported_devices() {
        let db = database_proxy::DatabaseProxy::new().expect("Could not connect to DB");
        let res = db
            .get_device_exported("*")
            .expect("Error running get_device_exported");
        assert!(res.len() > 0, "There should at least be one device");
        assert_eq!(res[0], "dserver/DataBaseds/2".to_string());
    }
}
