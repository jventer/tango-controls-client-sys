use crate::bridge::device_proxy_ffi::{
    self, BridgedAttributeData, BridgedBool, BridgedCmdArgTypeData, CmdArgType, DevState,
};
use crate::types::{AttrValue, AttributeInfo, CommandData, CommandInfo, DevEncoded};
use crate::types::{AttributeData, DevAttrEnum};
use anyhow::{anyhow, Result};
use cxx::UniquePtr;

pub struct DeviceProxy {
    device_proxy: UniquePtr<device_proxy_ffi::BridgedDeviceProxy>,
    pub device_name: String,
}

impl DeviceProxy {
    pub fn new(device_name: &str) -> Result<Self> {
        let dp = device_proxy_ffi::new_device_proxy(device_name)?;
        Ok(DeviceProxy {
            device_proxy: dp,
            device_name: device_name.to_string(),
        })
    }

    pub fn ping(&self) -> Result<i32> {
        let ping_result = self.device_proxy.ping()?;
        Ok(ping_result)
    }

    pub fn state(&self) -> Result<DevState> {
        let dev_state = self.device_proxy.state()?;
        Ok(dev_state)
    }

    pub fn status(&self) -> Result<String> {
        let status_result = self.device_proxy.status()?;
        Ok(status_result)
    }

    pub fn command_query(&self, command_name: &str) -> Result<CommandInfo> {
        let command_info_result = self.device_proxy.command_query(command_name)?;
        Ok(command_info_result.into())
    }

    pub fn command_inout(
        &self,
        command_name: &str,
        command_data: CommandData,
    ) -> Result<CommandData> {
        if command_name.to_lowercase() == "state" {
            let result_state = self.device_proxy.state()?;
            return Ok(CommandData::DevState(result_state));
        }

        if command_name.to_lowercase() == "status" {
            let result_status = self.device_proxy.status()?;
            return Ok(CommandData::String(result_status));
        }

        let mut bridged_device_data: BridgedCmdArgTypeData = BridgedCmdArgTypeData::default();

        let bridged_device_data = match command_data {
            CommandData::Void => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVoid;
                bridged_device_data
            }

            // data_Boolean: bool,
            CommandData::Boolean(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevBoolean;
                match command_arg {
                    true => bridged_device_data.data_Boolean = BridgedBool::BridgedTrue,
                    false => bridged_device_data.data_Boolean = BridgedBool::BridgedFalse,
                }
                bridged_device_data
            }

            // data_UShort: i16,
            CommandData::Short(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevShort;
                bridged_device_data.data_Short = command_arg;
                bridged_device_data
            }

            // data_Short: u16,
            CommandData::UShort(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevUShort;
                bridged_device_data.data_UShort = command_arg;
                bridged_device_data
            }

            // data_Long: i32,
            CommandData::Long(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevLong;
                bridged_device_data.data_Long = command_arg;
                bridged_device_data
            }

            // data_ULong: u32,
            CommandData::ULong(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevULong;
                bridged_device_data.data_ULong = command_arg;
                bridged_device_data
            }

            // data_Long64: i64,
            CommandData::Long64(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevLong64;
                bridged_device_data.data_Long64 = command_arg;
                bridged_device_data
            }

            // data_ULong64: u64,
            CommandData::ULong64(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevULong64;
                bridged_device_data.data_ULong64 = command_arg;
                bridged_device_data
            }

            // data_DevState: DevState,
            // CommandData::Long(command_arg) => {
            //     bridged_device_data.populated_command_arg_type = CmdArgType::Long;
            //     bridged_device_data.data_Long = command_arg;
            //     bridged_device_data
            // }

            // data_DevEncoded: Vec<String>,
            // CommandData::Long(command_arg) => {
            //     bridged_device_data.populated_command_arg_type = CmdArgType::Long;
            //     bridged_device_data.data_Long = command_arg;
            //     bridged_device_data
            // }

            // data_Float: f32,
            CommandData::Float(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevFloat;
                bridged_device_data.data_Float = command_arg;
                bridged_device_data
            }

            // data_Double: f64,
            CommandData::Double(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevDouble;
                bridged_device_data.data_Double = command_arg;
                bridged_device_data
            }

            // data_String: String,
            CommandData::String(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevString;
                bridged_device_data.data_String = command_arg;
                bridged_device_data
            }

            // data_UChar: u8,
            CommandData::Char(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevUChar;
                bridged_device_data.data_UChar = command_arg;
                bridged_device_data
            }

            // data_BooleanArray: Vec<BridgedBool>,
            // CommandData::BooleanArray(command_arg) => {
            //     bridged_device_data.populated_command_arg_type =
            //         CmdArgType::BooleanArray;
            //     bridged_device_data.data_BooleanArray = command_arg;
            //     bridged_device_data
            // }

            // data_CharArray: Vec<u8>,
            CommandData::CharArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarCharArray;
                bridged_device_data.data_CharArray = command_arg;
                bridged_device_data
            }

            // data_StringArray: Vec<String>,
            CommandData::StringArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarStringArray;
                bridged_device_data.data_StringArray = command_arg;
                bridged_device_data
            }

            // data_ShortArray: Vec<i16>,
            CommandData::ShortArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarShortArray;
                bridged_device_data.data_ShortArray = command_arg;
                bridged_device_data
            }

            // data_UShortArray: Vec<u16>,
            CommandData::UShortArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarUShortArray;
                bridged_device_data.data_UShortArray = command_arg;
                bridged_device_data
            }

            // data_LongArray: Vec<i32>,
            CommandData::LongArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarLongArray;
                bridged_device_data.data_LongArray = command_arg;
                bridged_device_data
            }

            // data_ULongArray: Vec<u32>,
            CommandData::ULongArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarULongArray;
                bridged_device_data.data_ULongArray = command_arg;
                bridged_device_data
            }

            // data_Long64Array: Vec<i64>,
            CommandData::Long64Array(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarLong64Array;
                bridged_device_data.data_Long64Array = command_arg;
                bridged_device_data
            }

            // data_ULong64Array: Vec<u64>,
            CommandData::ULong64Array(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarULong64Array;
                bridged_device_data.data_ULong64Array = command_arg;
                bridged_device_data
            }

            // data_FloatArray: Vec<f32>,
            CommandData::FloatArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarFloatArray;
                bridged_device_data.data_FloatArray = command_arg;
                bridged_device_data
            }

            // data_DoubleArray: Vec<f64>,
            CommandData::DoubleArray(command_arg) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarDoubleArray;
                bridged_device_data.data_DoubleArray = command_arg;
                bridged_device_data
            }

            CommandData::LongStringArray(long_arr, str_arr) => {
                bridged_device_data.populated_command_arg_type = CmdArgType::DevVarLongStringArray;
                bridged_device_data.data_LongArray = long_arr;
                bridged_device_data.data_StringArray = str_arr;
                bridged_device_data
            }

            CommandData::DoubleStringArray(double_arr, str_arr) => {
                bridged_device_data.populated_command_arg_type =
                    CmdArgType::DevVarDoubleStringArray;
                bridged_device_data.data_DoubleArray = double_arr;
                bridged_device_data.data_StringArray = str_arr;
                bridged_device_data
            }

            _ => return Err(anyhow!("No match for data type")),
        };
        let command_info_result = self
            .device_proxy
            .command_inout(command_name, bridged_device_data)?;

        let res: CommandData = match command_info_result.populated_command_arg_type {
            CmdArgType::DevVoid => CommandData::Void,
            CmdArgType::DevBoolean => match command_info_result.data_Boolean {
                BridgedBool::BridgedFalse => CommandData::Boolean(false),
                BridgedBool::BridgedTrue => CommandData::Boolean(true),
                _ => return Err(anyhow!("Unknown BridgedBool match")),
            },
            CmdArgType::DevShort => CommandData::Short(command_info_result.data_Short),
            CmdArgType::DevUShort => CommandData::UShort(command_info_result.data_UShort),
            CmdArgType::DevLong => CommandData::Long(command_info_result.data_Long),
            CmdArgType::DevULong => CommandData::ULong(command_info_result.data_ULong),
            CmdArgType::DevLong64 => CommandData::Long64(command_info_result.data_Long64),
            CmdArgType::DevULong64 => CommandData::ULong64(command_info_result.data_ULong64),
            CmdArgType::DevFloat => CommandData::Float(command_info_result.data_Float),
            CmdArgType::DevDouble => CommandData::Double(command_info_result.data_Double),
            CmdArgType::DevString => CommandData::String(command_info_result.data_String),
            CmdArgType::DevUChar => CommandData::Char(command_info_result.data_UChar),
            CmdArgType::DevVarCharArray => {
                CommandData::CharArray(command_info_result.data_CharArray)
            }
            CmdArgType::DevVarStringArray => {
                CommandData::StringArray(command_info_result.data_StringArray)
            }
            CmdArgType::DevVarShortArray => {
                CommandData::ShortArray(command_info_result.data_ShortArray)
            }
            CmdArgType::DevVarUShortArray => {
                CommandData::UShortArray(command_info_result.data_UShortArray)
            }
            CmdArgType::DevVarLongArray => {
                CommandData::LongArray(command_info_result.data_LongArray)
            }
            CmdArgType::DevVarULongArray => {
                CommandData::ULongArray(command_info_result.data_ULongArray)
            }
            CmdArgType::DevVarLong64Array => {
                CommandData::Long64Array(command_info_result.data_Long64Array)
            }
            CmdArgType::DevVarULong64Array => {
                CommandData::ULong64Array(command_info_result.data_ULong64Array)
            }
            CmdArgType::DevVarFloatArray => {
                CommandData::FloatArray(command_info_result.data_FloatArray)
            }
            CmdArgType::DevVarDoubleArray => {
                CommandData::DoubleArray(command_info_result.data_DoubleArray)
            }
            CmdArgType::DevVarLongStringArray => CommandData::LongStringArray(
                command_info_result.data_LongArray,
                command_info_result.data_StringArray,
            ),
            CmdArgType::DevVarDoubleStringArray => CommandData::DoubleStringArray(
                command_info_result.data_DoubleArray,
                command_info_result.data_StringArray,
            ),

            _ => return Err(anyhow!("No match for result")),
        };
        Ok(res)
    }

    pub fn get_command_list(&self) -> Result<Vec<String>> {
        let command_list = self.device_proxy.get_command_list()?;
        Ok(command_list)
    }

    pub fn get_attribute_list(&self) -> Result<Vec<String>> {
        let attribute_list = self.device_proxy.get_attribute_list()?;
        Ok(attribute_list)
    }

    pub fn read_attribute(&self, attribute_name: &str) -> Result<AttributeData> {
        let bridged_attribute: BridgedAttributeData =
            self.device_proxy.read_attribute(attribute_name)?;
        let new_attr_value: AttrValue = match bridged_attribute.data.populated_command_arg_type {
            CmdArgType::DevBoolean => match bridged_attribute.data.data_Boolean {
                BridgedBool::BridgedTrue => AttrValue::Boolean(true),
                BridgedBool::BridgedFalse => AttrValue::Boolean(false),
                _ => return Err(anyhow!("Not a valid BridgedBool")),
            },
            CmdArgType::DevVarBooleanArray => {
                let mut new_vec: Vec<bool> = Vec::new();
                for bb in bridged_attribute.data.data_BooleanArray.iter() {
                    match *bb {
                        BridgedBool::BridgedFalse => new_vec.push(false),
                        BridgedBool::BridgedTrue => new_vec.push(true),
                        _ => return Err(anyhow!("Invalid arm for BridgeBool")),
                    }
                }
                AttrValue::BooleanArray(new_vec)
            }
            CmdArgType::DevShort => AttrValue::Short(bridged_attribute.data.data_Short),
            CmdArgType::DevVarShortArray => {
                AttrValue::ShortArray(bridged_attribute.data.data_ShortArray)
            }
            CmdArgType::DevUShort => AttrValue::UShort(bridged_attribute.data.data_UShort),
            CmdArgType::DevVarUShortArray => {
                AttrValue::UShortArray(bridged_attribute.data.data_UShortArray)
            }
            CmdArgType::DevLong => AttrValue::Long(bridged_attribute.data.data_Long),
            CmdArgType::DevVarLongArray => {
                AttrValue::LongArray(bridged_attribute.data.data_LongArray)
            }
            CmdArgType::DevULong => AttrValue::ULong(bridged_attribute.data.data_ULong),
            CmdArgType::DevVarULongArray => {
                AttrValue::ULongArray(bridged_attribute.data.data_ULongArray)
            }
            CmdArgType::DevLong64 => AttrValue::Long64(bridged_attribute.data.data_Long64),
            CmdArgType::DevVarLong64Array => {
                AttrValue::Long64Array(bridged_attribute.data.data_Long64Array)
            }
            CmdArgType::DevULong64 => AttrValue::ULong64(bridged_attribute.data.data_ULong64),
            CmdArgType::DevVarULong64Array => {
                AttrValue::ULong64Array(bridged_attribute.data.data_ULong64Array)
            }
            CmdArgType::DevState => AttrValue::DevState(bridged_attribute.data.data_DevState),
            CmdArgType::DevEncoded => {
                // Combine data_String and data_CharArray for Vec<(String, Vec<u8>)>
                let mut dev_encoded = DevEncoded::default();
                dev_encoded.0 = bridged_attribute.data.data_String;
                dev_encoded.1 = bridged_attribute.data.data_CharArray;
                AttrValue::DevEncoded(dev_encoded)
            }
            CmdArgType::DevFloat => AttrValue::Float(bridged_attribute.data.data_Float),
            CmdArgType::DevVarFloatArray => {
                AttrValue::FloatArray(bridged_attribute.data.data_FloatArray)
            }
            CmdArgType::DevDouble => AttrValue::Double(bridged_attribute.data.data_Double),
            CmdArgType::DevVarDoubleArray => {
                AttrValue::DoubleArray(bridged_attribute.data.data_DoubleArray)
            }
            CmdArgType::DevString => AttrValue::String(bridged_attribute.data.data_String.clone()),
            CmdArgType::DevVarStringArray => {
                AttrValue::StringArray(bridged_attribute.data.data_StringArray.clone())
            }
            CmdArgType::DevUChar => AttrValue::UChar(bridged_attribute.data.data_UChar),
            CmdArgType::DevVarCharArray => {
                AttrValue::UCharArray(bridged_attribute.data.data_CharArray)
            }
            CmdArgType::DevEnum => AttrValue::DevEnum(DevAttrEnum {
                labels: bridged_attribute.data.data_enum_labels,
                enum_value: bridged_attribute.data.data_DevEnum,
            }),
            _ => return Err(anyhow!("No data available")),
        };

        Ok(AttributeData {
            data: new_attr_value,
            format: bridged_attribute.format,
            quality: bridged_attribute.quality,
            name: bridged_attribute.name,
            dim_x: bridged_attribute.dim_x,
            dim_y: bridged_attribute.dim_y,
            time_stamp_tv_sec: bridged_attribute.time_stamp_tv_sec,
            time_stamp_tv_usec: bridged_attribute.time_stamp_tv_usec,
            time_stamp_tv_nsec: bridged_attribute.time_stamp_tv_nsec,
        })
    }

    pub fn attribute_list_query(&self) -> Result<Vec<AttributeInfo>> {
        let bridged_attr_infos = self.device_proxy.attribute_list_query()?;
        let mut attr_infos: Vec<AttributeInfo> = Vec::new();
        for bridged_attr_info in bridged_attr_infos.iter() {
            let attr_info: AttributeInfo = bridged_attr_info.into();
            attr_infos.push(attr_info);
        }
        Ok(attr_infos)
    }

    pub fn command_list_query(&self) -> Result<Vec<CommandInfo>> {
        let bridged_command_list = self.device_proxy.command_list_query()?;
        let mut commands_info: Vec<CommandInfo> = Vec::new();
        for bridged_command_info in bridged_command_list.into_iter() {
            let command_info: CommandInfo = bridged_command_info.into();
            commands_info.push(command_info);
        }
        Ok(commands_info)
    }
}
