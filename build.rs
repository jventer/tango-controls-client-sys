use std::process;

fn main() {
    let tango_lib = match pkg_config::probe_library("tango") {
        Ok(lib) => lib,
        Err(err) => {
            print!("{}---", err);
            process::exit(1);
        }
    };

    let mut conf = cxx_build::bridge("src/bridge.rs");
    conf.file("src/device_proxy.cpp");
    conf.file("src/database_proxy.cpp");
    conf.flag_if_supported("-std=c++17");
    for path in tango_lib.include_paths {
        println!("cargo:warning=Including dir {:?}", &path);
        conf.include(&path);
        // Add in tango path if exists
        let tango_path = path.join("tango");
        if tango_path.exists() {
            conf.include(&tango_path);
            println!("cargo:warning=Including Tango dir {:?}", &tango_path);
        }
    }
    conf.compile("tango-controls-client-sys");

    println!("cargo:rerun-if-changed=src/device_proxy.rs");
    println!("cargo:rerun-if-changed=src/lib.rs");
    println!("cargo:rerun-if-changed=src/device_proxy.cpp");
    println!("cargo:rerun-if-changed=src/database_proxy.cpp");
    println!("cargo:rerun-if-changed=src/include/device_proxy.h");
    println!("cargo:rerun-if-changed=src/include/database_proxy.h");
    println!("cargo:rustc-link-lib=omnithread");
    println!("cargo:rustc-link-lib=omniORB4");
    println!("cargo:rustc-link-lib=tango");
    println!("cargo:rustc-link-lib=COS4");
    println!("cargo:rustc-link-lib=omniDynamic4");
    println!("cargo:rustc-link-lib=zmq");
}
