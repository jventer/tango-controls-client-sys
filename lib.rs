mod database_proxy;
mod device_proxy;

use crate::device_proxy::ffi::{new_device_proxy, ping_device, run_str_command};

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_works() {
        let res = new_device_proxy();
    }

    #[test]
    fn test_ping() {
        let res = ping_device();
        assert!(res > 0);
    }

    #[test]
    fn test_str_command() {
        let arg: String = String::from("A String");
        let res: String = run_str_command(arg.clone());
        assert_eq!(arg, res);
    }
}
